// -> MongoClient
import { Collection, Db, MongoClient } from "mongodb"
// -> Interfaces
import { Movie } from '../model/movies'

interface MoviesService {
    [key:string]: any
}

interface Params {
    [key:string]: Array<string>
}

// Variables and Constants
var db: Db
var collections: { movies?: Collection<Movie> } = {}
const moviesService:MoviesService  = {}

// Functions
moviesService.injectDB = async (client:MongoClient) => {
    if (db) return
    try {
        await client.connect()
        db = client.db('sample_mflix')
        collections.movies = db.collection('movies')
    } catch (e) {
        console.error(e);
        throw `No se puedo establecer una conexión con la bd en MoviesService: ${e}`
    }
}

moviesService.findByTitle = async (title:string) => {

    if (!collections.movies) return null

    const movie = collections.movies?.findOne({ title });

    return movie
}

moviesService.filter = async (params:Params): Promise<Movie[]> => {

    const {
        countries,
        genres,
        languages,
        year
    } = params

    console.log(languages)

    let cursor = await collections.movies?.find({
        $or: [
            {countries: {$in: countries }},
            {genres: { $in: genres }},
            {languages: { $in: languages }},
            {year: {$gte: year}}
        ]
    }).limit(10)

    const movies: Array<Movie> = []

    await cursor?.forEach(c => {
        movies.push(c as Movie)
    });

    return movies
}

export default moviesService