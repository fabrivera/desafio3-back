import { Request, Response } from "express";
import moviesService from "./movies.service";

interface MoviesController {
    [key:string]: (req:Request, res:Response) => {}
}

const moviesController: MoviesController = {}

moviesController.findbyTitle = async (req, res): Promise<Response> => {
    const { title } = req.query
    if (!title) return res.status(400).send()
    const movie = await moviesService.findByTitle(title)
    if (!movie) return res.status(404).send(`No se encontró el película con titulo: ${title}`)
    return res.json(movie)
}

moviesController.filter = async (req, res): Promise<Response> => {

    if (
        !(req.query.countries) && 
        !(req.query.genres) &&
        !(req.query.languages) &&
        !(req.query.year) 
    ) return res.status(400).send()
    
    const countries = typeof req?.query?.countries === "string" ? req.query.countries.split(",") : []
    const genres = typeof req?.query?.genres === "string" ? req.query.genres.split(",") : []
    const languages = typeof req?.query?.languages === "string" ? req.query.languages.split(",") : []
    const year = req.query.year

    const params = {
        countries,
        genres,
        languages,
        year: Number(year)
    }

    try {
        const movies = await moviesService.filter(params)
        return res.json(movies)
    }
    catch (error) {
        return res.status(404).send(`No se encontraron pelíulas`)
    }
}

export default moviesController