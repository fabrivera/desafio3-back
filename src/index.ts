// ---- CONFIGURATION ----
    // -> Express
    import express from 'express'
        const app = express()
    // -> Node libreries
    //import fs from "fs"
    // -> Enviropment varibles
    import dotenv from 'dotenv'
        dotenv.config()
    // -> MongoClient
    import { MongoClient } from "mongodb"
    // -> Import Services and Controllers
    //import MoviesController from './api/movies.controller'
    import moviesService from './api/movies.service'
    import moviesController from './api/movies.controller'
    const { findbyTitle, filter } = moviesController // Recuperando funciones del controlador
    // * Port
    const port = process.env.PORT || 4000

// ---- MIDDLEWARES ----
app.use(express.json())

// ---- ROUTES ----
app.get('/movie', findbyTitle)
app.get('/movies', filter)

// ---- START CONNECTION ----
MongoClient.connect(process.env.DB_URI || '')
    .then(async client => {
        await moviesService.injectDB(client)
        app.listen(port, () => {
            console.log(`Server corriendo en http://localhost:${port}`)
        })
    })
    .catch(err => {
        console.error(err.stack)
        process.exit(1)
    })