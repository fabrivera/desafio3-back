"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// Variables and Constants
var db;
var collections = {};
const moviesService = {};
// Functions
moviesService.injectDB = (client) => __awaiter(void 0, void 0, void 0, function* () {
    if (db)
        return;
    try {
        yield client.connect();
        db = client.db('sample_mflix');
        collections.movies = db.collection('movies');
    }
    catch (e) {
        console.error(e);
        throw `No se puedo establecer una conexión con la bd en MoviesService: ${e}`;
    }
});
moviesService.findByTitle = (title) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    if (!collections.movies)
        return null;
    const movie = (_a = collections.movies) === null || _a === void 0 ? void 0 : _a.findOne({ title });
    return movie;
});
moviesService.filter = (params) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const { countries, genres, languages, year } = params;
    console.log(languages);
    let cursor = yield ((_b = collections.movies) === null || _b === void 0 ? void 0 : _b.find({
        $or: [
            { countries: { $in: countries } },
            { genres: { $in: genres } },
            { languages: { $in: languages } },
            { year: { $gte: year } }
        ]
    }).limit(10));
    const movies = [];
    yield (cursor === null || cursor === void 0 ? void 0 : cursor.forEach(c => {
        movies.push(c);
    }));
    return movies;
});
exports.default = moviesService;
