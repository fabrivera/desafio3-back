"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const movies_service_1 = __importDefault(require("./movies.service"));
const moviesController = {};
moviesController.findbyTitle = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { title } = req.query;
    if (!title)
        return res.status(400).send();
    const movie = yield movies_service_1.default.findByTitle(title);
    if (!movie)
        return res.status(404).send(`No se encontró el película con titulo: ${title}`);
    return res.json(movie);
});
moviesController.filter = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c;
    if (!(req.query.countries) &&
        !(req.query.genres) &&
        !(req.query.languages) &&
        !(req.query.year))
        return res.status(400).send();
    const countries = typeof ((_a = req === null || req === void 0 ? void 0 : req.query) === null || _a === void 0 ? void 0 : _a.countries) === "string" ? req.query.countries.split(",") : [];
    const genres = typeof ((_b = req === null || req === void 0 ? void 0 : req.query) === null || _b === void 0 ? void 0 : _b.genres) === "string" ? req.query.genres.split(",") : [];
    const languages = typeof ((_c = req === null || req === void 0 ? void 0 : req.query) === null || _c === void 0 ? void 0 : _c.languages) === "string" ? req.query.languages.split(",") : [];
    const year = req.query.year;
    const params = {
        countries,
        genres,
        languages,
        year: Number(year)
    };
    try {
        const movies = yield movies_service_1.default.filter(params);
        return res.json(movies);
    }
    catch (error) {
        return res.status(404).send(`No se encontraron pelíulas`);
    }
});
exports.default = moviesController;
