"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// ---- CONFIGURATION ----
// -> Express
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
// -> Node libreries
//import fs from "fs"
// -> Enviropment varibles
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
// -> MongoClient
const mongodb_1 = require("mongodb");
// -> Import Services and Controllers
//import MoviesController from './api/movies.controller'
const movies_service_1 = __importDefault(require("./api/movies.service"));
const movies_controller_1 = __importDefault(require("./api/movies.controller"));
const { findbyTitle, filter } = movies_controller_1.default; // Recuperando funciones del controlador
// * Port
const port = process.env.PORT || 4000;
// ---- MIDDLEWARES ----
app.use(express_1.default.json());
// ---- ROUTES ----
app.get('/movie', findbyTitle);
app.get('/movies', filter);
// ---- START CONNECTION ----
mongodb_1.MongoClient.connect(process.env.DB_URI || '')
    .then((client) => __awaiter(void 0, void 0, void 0, function* () {
    yield movies_service_1.default.injectDB(client);
    app.listen(port, () => {
        console.log(`Server corriendo en http://localhost:${port}`);
    });
}))
    .catch(err => {
    console.error(err.stack);
    process.exit(1);
});
